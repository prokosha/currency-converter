const fromSelect = document.getElementById("from");
const toSelect = document.getElementById("to");
class Currency {
    constructor (name, courseBuy, courseSell) {
        this.name = name;
        this.courseBuy = +courseBuy;
        this.courseSell = +courseSell;
        document.querySelector('.converter').addEventListener('input', this.converterCurrency.bind(this));
    }
    converterCurrency() {
        const {value} = document.getElementById("inputSum");
        const result = document.getElementById("result");
        const courseField =  document.getElementById("course");
        if(isNaN(value) || (+value) === 0){
            courseField.value = result.value = "";
            return;
         }
        const checkedValue = document.querySelector('input[name="buySell"]:checked').value;
        const curFrom = mapCurrency[fromSelect.options.selectedIndex];
        const curTo = mapCurrency[toSelect.options.selectedIndex];
        let ratioCurrency = checkedValue === "buy" ? curFrom.courseSell / curTo.courseBuy : curFrom.courseBuy / curTo.courseSell;
        let res = curFrom.name === curTo.name ? value : value * ratioCurrency;
        let course = curFrom.name === "UAH" ? value / res : res / value;
        result.value = Math.round(res * 10000) / 10000;
        courseField.value = Math.round(course * 100000) / 100000;        
    }
}
const mapCurrency = [
    new Currency("UAH", 1, 1)
];
class DataBank{
    constructor(data, arrCurrency){
        this.api = data.url;
        this.nameCur = data.valueFields[0];
        this.curBuy = data.valueFields[1];
        this.curSell = data.valueFields[2];
        this.arrCurrency = arrCurrency;
        this.getDataBank();
    }
    getDataBank (){
        fetch(this.api)
            .then(res => res.json())
            .then(dataFromBank => {
                dataFromBank.forEach(el => {
                    if (el[this.nameCur] === "RUR"){
                        el[this.nameCur] = "RUB";
                    }
                    this.arrCurrency.push(new Currency(el[this.nameCur], el[this.curBuy], el[this.curSell]))
                });
                this.arrCurrency.forEach(el =>{
                    const  option = new Option(el.name);
                    fromSelect.appendChild(option);
                    const optionTwo = new Option(el.name);
                    toSelect.appendChild(optionTwo);
                });
            })
            .catch(error => {
                alert("Загрузка курсов валют не удалась!!!"); 
            });
    }
}
const dataFromPB = {
    url: 'https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5',
    valueFields: ["ccy", "buy", "sale"]
}
new DataBank(dataFromPB, mapCurrency);


